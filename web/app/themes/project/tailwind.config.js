/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "**/*.php",
    "./src/**/*.{html,js,jsx,vue,ts,tsx}"
  ],
  corePlugins: {
    container: false
  },
  theme: {
    screens: {
      'xs': '0px',
      'sm': '576px',
      'md': '768px',
      'lg': '1024px',
      'xl': '1350px',
      'xxl': '1600px',
    },
    extend: {},
  },
  plugins: [],
}
