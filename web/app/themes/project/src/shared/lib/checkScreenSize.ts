import {useEffect} from 'react';


export const isScreenWidth = (size?: string, callback?: () => void) => {

    const breackpoints = {
        xs: 0,
        sm: 576,
        md: 768,
        lg: 1024,
        xl: 1350,
        xxl: 1600,
    }

    const data: any = {
        sizes: {
            xs: false,
            sm: false,
            md: false,
            lg: false,
            xl: false,
            xxl: false,
        },
        mobile: false,
        pc: false,
    }

    function checkSize(size: number, type: string = 'min') {
        return window.matchMedia(`(${type}-width: ${size}px)`).matches
    }

    function callbackHandler() { callback() }


    useEffect(() => {
        const handleClick = () => {
            data.sizes.xs = checkSize(breackpoints.xs) && !checkSize(breackpoints.sm)
            data.sizes.sm = checkSize(breackpoints.sm) && !checkSize(breackpoints.md)
            data.sizes.md = checkSize(breackpoints.md) && !checkSize(breackpoints.lg)
            data.sizes.lg = checkSize(breackpoints.lg) && !checkSize(breackpoints.xl)
            data.sizes.xl = checkSize(breackpoints.xl) && !checkSize(breackpoints.xxl)
            data.sizes.xxl = checkSize(breackpoints.xxl)
            data.mobile = !checkSize(breackpoints.lg)
            data.pc = checkSize(breackpoints.lg)

            if(!!callback){
                if(size in data.sizes){ data.sizes[size] && callbackHandler() }
                if(size in data){ data[size] && callbackHandler() }
            }
        };

        window.addEventListener('load', handleClick, true);
        window.addEventListener('resize', handleClick, true);

        return () => {
            window.removeEventListener('resize', handleClick, true);
        };
    });

    if(!!size){
        if(size in data.sizes){ return data.sizes[size] }
        if(size in data){ return data[size] }
        else { console.error(`Size ${size} is not supported`) }
    }
    else {  return data  }
};
