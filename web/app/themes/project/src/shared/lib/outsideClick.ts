import {useRef, useEffect} from 'react';

export const useOutsideClick = (elem: any, callback?: () => void) => {

    useEffect(() => {
        const handleClick = (event: any) => {

            if (elem.current && !elem.current.contains(event.target)) {
                !!callback && callback();
            }

        };

        document.addEventListener('click', handleClick, true);

        return () => {
            document.removeEventListener('click', handleClick, true);
        };
    });
};
