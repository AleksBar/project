export const parseProps = (prop: any) => {
    // Парсим строку пропсов
    if (typeof prop === 'string') {
        try {
            return JSON.parse(prop);
        }
        catch (e) {
            return prop;
        }
    }

    return prop;
}
export const parseAllProps = (props: any) => {
    const data : any = {}

    if (props) {
        for (const propsKey in props) {
            data[propsKey] = parseProps(props[propsKey])
        }
    }

    return data;
}