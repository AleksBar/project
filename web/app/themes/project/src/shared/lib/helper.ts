export const breakpoints: Record<string, number> = {
    xs: 0,
    sm: 576,
    md: 768,
    lg: 1024,
    xl: 1350,
    xxl: 1600,
}
export function menuSort(menu: any, order: string, entries: string = 'items'){
    let buffer: any[] = []
    if(typeof menu === "object"){
        Object.keys(menu).map((key: string) => {
            buffer.push(menu[key])
        })
        buffer = buffer.sort( (a, b) => a[order] - b[order])

        buffer.map(item => {
            if(item[entries]){
                item[entries] = menuSort(item[entries], order)
            }
        })
        return buffer
    }
}
