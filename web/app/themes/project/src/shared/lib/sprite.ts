function requireAll(r: __WebpackModuleApi.RequireContext) {
    r.keys().forEach(r);
}

export function generateSpriteSvg() {
    requireAll(require.context(__PATH_TO_SPRITE__, true, /\.svg$/));

    if (site.themeUrl) {
        const url = __IS_DEV__
            ? 'http://' + __HOST__ + ':' + __PORT__ + __PATH_TO_THEMES__ + '/' + __THEME_NAME__ + '/' + site.buildDir + '/sprite.svg'
            : `${site.themeUrl}/${site.buildDir}/sprite.svg`

        fetch(url)
            .then(res => {
                return res.text();
            })
            .then(data => {
                const containerSvg = document.createElement('div');
                containerSvg.setAttribute('id', 'svg-icons');
                containerSvg.style.display = 'none';
                document.body.insertAdjacentElement("beforeend", containerSvg)
                containerSvg.innerHTML = data;
            })
            .then(
                () => {
                    const svg = document.querySelectorAll('.icon_svg')
                }
            );
    } else {
        const svg = document.querySelectorAll('.icon_svg')

        if (svg.length) {
            svg.forEach((item: HTMLElement) => {
                item.style.display = 'none';
            })
        }
    }
}
