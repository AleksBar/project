import { Base64 } from 'js-base64';
import * as crypto from "crypto";
import OAuth, {Authorization} from "oauth-1.0a";

function hash_function_sha1(base_string: string, key: string) {
    return crypto
        .createHmac('sha1', key)
        .update(base_string)
        .digest('base64')
}

export async function fetchClient(query: string, lang: string = 'ru', params?: FormData | Record<string, string | number>, method: string = 'GET') {
    // Объявляем базовый путь
    const basePath = '/wp-json/'

    // объявляем переменную сбора пути
    let path = '';

    // Определяем язык сайта
    if (lang !== 'ru' && lang !== '' && lang !== null) {
        path += `/${lang}`
    }

    // Объявляем настройки запроса
    const querySettings = {
        method,
        url: window.location.origin + path + basePath + query,
    }

    // Создаем обьект авторизации
    const oauth = new OAuth({
        consumer: {key: __CK__, secret: __CS__},
        signature_method: 'HMAC-SHA1',
        nonce_length: 10,
        hash_function: hash_function_sha1,
    })

    // Объявляем переменную ответа
    let response

    if (method === 'GET' || method === 'get') {
        // Объявляем параметры

        // @ts-ignore
        let queryParams = new URLSearchParams(params);

        if (params) {
            querySettings.url += '?' + queryParams.toString()
        }

        response = await fetch(querySettings.url, {
            ...querySettings,
            headers: {
                "X-WP-Nonce": site.nonce,
                ...oauth.toHeader(oauth.authorize(querySettings))
                // "Authorization" : 'Basic ' + Base64.btoa(__CK__ + ':' + __CS__)
            }
        })

    } else {
        const formData = params instanceof FormData ? params : JSON.stringify(params)

        response = await fetch(querySettings.url, {
            ...querySettings,
            headers: {
                "X-WP-Nonce": site.nonce,
                // ...oauth.toHeader(oauth.authorize(querySettings))
                "Authorization" : 'Basic ' + Base64.btoa(__CK__ + ':' + __CS__)
            },
            body: formData
        })
    }

    const resHeaders: Record<string, any> = {}
    // @ts-ignore
    for (let [key, value] of response.headers) {
        resHeaders[key] = value;
    }
    const res = await response.json()
    const answer = {
        status: response.status,
        data: res,
        headers: resHeaders,
    }

    if (!response.ok) {
        throw answer
    }

    return answer
}

export async function fetchPage(id: string, lang: string = 'ru') {
    return (await fetchClient( `wp/v2/pages/${id}`, lang)).data
}

export async function fetchPageBySlag(slug: string, lang: string = 'ru') {
    return (await fetchClient( `wp/v2/pages/`, lang, {
        slug
    })).data
}

export async function fetchMenus(lang: string = 'ru') {
    return (await fetchClient(`wp/v2/menus`, lang)).data
}

export async function fetchMenuById(id: string, lang: string = 'ru') {
    return (await fetchClient(`wp/v2/menu-items`, lang, {
        menus: id
    })).data
}

export async function fetchBreadCrumps(id: string, lang: string = 'ru') {
    return (await fetchClient(`rankmath/v1/breadcrumbs/${id}`, lang)).data
}

//Получение полей со страницы опций по id
export async function fetchOptionsById(id: string, lang: string = 'ru') {
    return (await fetchClient(`acf/v3/options/${id}`, lang)).data
}

export async function fetchPost(id: string, postType: string, lang: string = 'ru') {
    return (await fetchClient(`wp/v2/${postType}/${id}`, lang)).data
}
export async function fetchPosts(postType: string, params: Record<string, any> = {}, lang: string = 'ru') {
    return await fetchClient(`wp/v2/${postType}`, lang, params)

}

interface IFetchAcfFields {
    id: string;
    field?: string;
}

export async function fetchAcfFields(props: IFetchAcfFields) {
    const data = new FormData()

    props?.field !== undefined && data.append('field', props.field)
    props?.id !== undefined && data.append('id', props.id)
    !props && data.append('error', 'fetch_props_empty')

    const response = await fetch(`/wp-json/custom-api/v1/acf-fields/?_wpnonce=${site.nonce}`, {
        method: 'POST',
        body: data
    })

    if (!response.ok) {
        throw new Error('Network response was not ok')
    }

    return response.json()
}
