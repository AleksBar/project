declare module '*.scss' {
    interface IClassNames {
        [className: string]: string
    }
    const classNames: IClassNames;
    export = classNames;
}

declare module '*.svg' {
    import React from 'react';

    const SVG: React.VFC<React.SVGProps<SVGSVGElement>>;
    export default SVG;
}

declare const __IS_DEV__: boolean;
declare const __PATH_TO_SPRITE__: string;
declare const __PORT__: string;
declare const __HOST__: string;
declare const __PATH_TO_THEMES__: string;
declare const __THEME_NAME__: string;
declare const __CK__: string;
declare const __CS__: string;

declare const site: {
    ajaxurl: string;
    themeUrl: string;
    isHome: string;
    buildDir: string;
    nonce: string;
    loginNonce: string;
    isAdmin: boolean;
    default_events_category: string;
    userId: string | null;
    contacts: string;
    menu: string;
    bg_card: string;
};

declare module 'react-dom/client';
