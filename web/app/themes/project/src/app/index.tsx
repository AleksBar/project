import React from "react"
import * as ReactDOM from "react-dom"
import { generateSpriteSvg } from "shared/lib/sprite";
import "@fancyapps/ui/dist/fancybox.css";
// @ts-ignore
import { Fancybox } from "@fancyapps/ui";
// @ts-ignore
import reactToWebComponent from "react-to-webcomponent"

import './styles/index.scss'
import { Widgets } from "widgets";


// Генерация СВГ спрайта
generateSpriteSvg();

// Widgets.map(Component => {
//     const name = Component.name.replace(/[A-Z]/g, (letter: string) => `-${letter.toLowerCase()}`);
//     customElements.get(name) || customElements.define(name, reactToWebComponent(Component, React, ReactDOM, {dashStyleAttributes: true}));
// })
customElements.define('product-list', reactToWebComponent(Widgets.ProductList, React, ReactDOM, {dashStyleAttributes: true}))
