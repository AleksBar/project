import React, {FC, useState} from "react";
import classNames from "classnames";
import cls from "./Burger.module.scss"
import {Button, Icon} from "shared/ui";

interface BurgerProps {
    className?: string;
    onClick?: () => void
}

export const Burger: FC<BurgerProps> = (props) => {
    const {className, onClick} = props
    const [open, setOpen] = useState(false)

    const openBurger = () => {
        setOpen(!open)
        !!onClick && onClick()
    }

    return (
        <Button className={classNames(cls.burger, className)} onClick={openBurger}>
            {open && <Icon className={classNames(cls.openSvg, cls.svg)} name={'close'}/>}
            {!open && <Icon className={cls.svg} name={'burger'}/>}
        </Button>
    )
}
