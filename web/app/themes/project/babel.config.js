const plugins = [
    [
        'babel-plugin-typescript-to-proptypes',
        {
            comments: true,
            strict : false,
            implicitChildren: true
        }
    ],
    ["@babel/plugin-proposal-decorators", { "legacy": true }],
    ["@babel/plugin-proposal-class-properties", { "loose": true }]
];

module.exports = {
    presets: ['@babel/preset-typescript', '@babel/preset-react'],
    plugins,
};
