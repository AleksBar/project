<?php
CE::theLayout('header');

$post_type = get_post_type();
if ($post_type) {
    CE::theTemplate('template/single/single-' . $post_type);
}

CE::theLayout('footer');
