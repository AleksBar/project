import webpack from 'webpack';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import {BuildOptions} from './types/config';
import path from "path";

export function buildLoaders(options: BuildOptions): webpack.RuleSetRule[] {
    const {isDev, paths} = options;

    const stylesHandler = () => {
        return isDev ? 'style-loader' : MiniCssExtractPlugin.loader;
    };

    const typescriptLoader = {
        test: /\.tsx?$/,
        use: [
            'babel-loader'
        ],
        exclude: /node_modules/,
    };

    const lessLoader = {
        loader: "less-loader",
        options: {
            lessOptions: { // If you are using less-loader@5 please spread the lessOptions to options directly
                modifyVars: {
                    'primary-color': '#1f2d47',
                    'link-color': '#1b5bd2',
                    'border-radius-base': '8px',
                },
                javascriptEnabled: true,
            },
        },
    }

    const jsLoader = {
        test: /\.(js|jsx)$/i,
        use: [
            "babel-loader"
        ],
    }

    const styleLoader = {
            test: /\.s[ac]ss$/i,
            use: [
                stylesHandler(),
                {
                    loader: 'css-loader',
                    options: {
                        modules: {
                            auto: (resPath: string) => resPath.includes('.module'),
                            localIdentName: isDev ? '[path][name]__[local]--[hash:base64:5]' : '[hash:base64:8]',
                        },
                    },
                },
                'postcss-loader',
                'sass-loader',
            ],
        };

    const cssLoader = {
        test: /\.css$/i,
        use: [stylesHandler(), 'css-loader', 'postcss-loader'],
    }

    const imgLoader = {
        test: /\.(png|jpg|gif)$/i,
        use: [
            {
                loader: 'url-loader',
                options: {
                    limit: 8192,
                },
            },
        ],
        type: 'javascript/auto',
    };

    const fileLoader = {
        test: /\.(png|jpg|gif)$/i,
        type: "asset/resource",
        generator: {
            filename: 'img/[name][ext][query]'
        }
    }

    const svgLoader = {
        test: /\.svg$/,
        use: [
            {
                loader: 'svg-sprite-loader',
                options: {
                    extract: true,
                    outputPath: '/',
                    publicPath: options.buildDir
                }
            },
            'svg-transform-loader',
            'svgo-loader',
        ]
    };

    return [
        jsLoader,
        typescriptLoader,
        styleLoader,
        cssLoader,
        svgLoader,
        imgLoader,
        fileLoader,
    ];
}
