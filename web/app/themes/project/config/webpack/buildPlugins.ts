import webpack from 'webpack';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import WebpackAssetsManifest from 'webpack-assets-manifest';
import CopyPlugin from 'copy-webpack-plugin';
import { BuildOptions } from './types/config';
import path from "path";
import NodePolyfillPlugin from "node-polyfill-webpack-plugin"

const SpriteLoaderPlugin = require('svg-sprite-loader/plugin');

export function buildPlugins({ paths, isDev, port, host, themeName, allEnv }: BuildOptions): webpack.WebpackPluginInstance[] {
    return [
        new webpack.ProgressPlugin(),
        new NodePolyfillPlugin(),

        new MiniCssExtractPlugin({
            filename: isDev ? "css/[name].css" : "css/[name].[contenthash:8].css",
            chunkFilename: isDev ? "css/[name].css" : "css/[name].[contenthash:8].css",
        }),

        // Прокидываем переменные из webpack в js
        new webpack.DefinePlugin({
            __IS_DEV__: JSON.stringify(isDev),
            __VUE_OPTIONS_API__: true,
            __VUE_PROD_DEVTOOLS__: false,
            __PATH_TO_SPRITE__: JSON.stringify(path.resolve(paths.src, 'assets', 'icons')),
            __PORT__: JSON.stringify(port),
            __HOST__: JSON.stringify(host),
            __PATH_TO_THEMES__: JSON.stringify(paths.toThemes),
            __THEME_NAME__: JSON.stringify(themeName),
            __CK__: JSON.stringify(allEnv?.ck),
            __CS__: JSON.stringify(allEnv?.cs),
        }),

        new webpack.HotModuleReplacementPlugin(),
        new SpriteLoaderPlugin(),

        new WebpackAssetsManifest({
            output: 'assets.json',
            space: 2,
            writeToDisk: true,
            assets: {},
        }),

        new CopyPlugin({
            patterns: [
                {
                    from: path.resolve(paths.src, 'assets'),
                    to: path.resolve(paths.build, 'assets'),
                    globOptions: {
                        ignore: [
                            "**/.gitkeep",
                        ],
                    },
                    noErrorOnMissing: true,
                }
            ],
        })
    ];
}
