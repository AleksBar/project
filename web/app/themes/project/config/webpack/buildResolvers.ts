import { ResolveOptions } from 'webpack';
import { BuildOptions } from './types/config';
import path from "path";

export function buildResolvers(options: BuildOptions): ResolveOptions {
    return {
        extensions: [
            '.js',
            '.ts',
            '.tsx',
            '.jsx',
            '.css',
            '.scss',
        ],
        preferAbsolute: true,
        modules: [options.paths.src, 'node_modules'],
        mainFiles: ['index'],
        alias: {
            'icons': path.resolve(options.paths.src, 'assets', 'icons'),
            // "react": "preact/compat",
            // "react-dom/test-utils": "preact/test-utils",
            // "react-dom": "preact/compat",     // Must be below test-utils
            // "react/jsx-runtime": "preact/jsx-runtime"
        }
    };
}
