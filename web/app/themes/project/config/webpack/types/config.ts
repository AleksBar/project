import {EntryObject} from "webpack/types"

export type BuildMode = 'production' | 'development';

export interface BuildPaths {
    entry: string | EntryObject;
    build: string;
    html: string;
    src: string;
    toThemes: string;
    publicPath: string;
}

export interface BuildEnv {
    mode: BuildMode;
    port: number;
    domain: string;
    serve: boolean
}

export interface BuildOptions {
    mode: BuildMode;
    paths: BuildPaths;
    isDev: boolean;
    port: number;
    domain: string;
    themeName: string;
    buildDir: string;
    isServe: boolean;
    host: string;
    allEnv?: {
        cs: string,
        ck: string,
        userLogin: string,
        userPassword: string,
    }
}
