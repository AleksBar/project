import webpack from 'webpack';
import { BuildOptions } from './types/config';
import { buildPlugins } from './buildPlugins';
import { buildLoaders } from './buildLoaders';
import { buildResolvers } from './buildResolvers';
import { buildDevServer } from './buildDevServer';
import CssMinimizerPlugin from "css-minimizer-webpack-plugin";

export function buildWebpackConfig(options: BuildOptions): webpack.Configuration {
    const { paths, mode, isDev } = options;

    const config = {
        mode,
        entry: paths.entry,
        output: {
            filename: isDev ? 'js/[name].js' : 'js/[name].[contenthash:8].js',
            // publicPath: options.domain + ':' + options.port + options.paths.toThemes + '/' + options.themeName,
            path: paths.build,
            clean: true,
        },
        plugins: buildPlugins(options),
        module: {
            rules: buildLoaders(options),
        },
        resolve: buildResolvers(options),
        devtool: isDev ? 'inline-source-map' : undefined,
        devServer: isDev ? buildDevServer(options) : undefined,
        optimization: {}
    }

    if (!isDev) {
        config.optimization = {
            splitChunks: {
                cacheGroups: {
                    vendor: {
                        name: 'vendors',
                        test: /[\\/]node_modules[\\/]/,
                        chunks: 'all',
                        enforce: true
                    }
                }
            },
            minimizer: [
                new CssMinimizerPlugin(),
            ],
        }
    }

    return config;
}
