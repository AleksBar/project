import type { Configuration as DevServerConfiguration } from 'webpack-dev-server';
import { BuildOptions } from './types/config';

export function buildDevServer(options: BuildOptions): DevServerConfiguration {
    return {
        port: options.port,
        open: false,
        historyApiFallback: true,
        hot: true,
        host: 'localhost',
        allowedHosts: 'all',
        webSocketServer: 'ws',
        compress: true,
        client: {
            webSocketTransport: 'ws',
            progress: false,
            overlay: {
                errors: true,
                warnings: false,
            },
            logging: 'error',
        },
        devMiddleware: {
            publicPath: options.paths.publicPath,
        },
        watchFiles: ['**/*.php'],
        proxy: {
            '/': {
                target: `http://${options.domain}/`,
                secure: false,
                changeOrigin: true,
            }
        },
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
            'Access-Control-Allow-Headers': 'X-Requested-With, content-type, Authorization',
        },
    };
}
