<?php
require_once 'wordpress/variables.php';
require_once 'wordpress/theme.php';
require_once 'wordpress/wordpress.php';
require_once 'wordpress/component-engine.php';
require_once 'wordpress/assets.php';
require_once 'wordpress/acf-fields.php';
