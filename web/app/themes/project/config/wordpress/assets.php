<?php

add_action('wp_enqueue_scripts', function () {
    Assets::init(ASSETS_JSON, [
        'mode'            => THEME_MOD,
        'devToThemesUrl'  => 'http://' . DEV_HOST . ':' . DEV_PORT . PATH_TO_THEMES,
        'prodToThemesUrl' => get_home_url() . PATH_TO_THEMES,
        'themeName'       => THEME_DIRNAME,
        'buildDir'        => BUILD_DIR,
    ]);

    Assets::registerChunks();

    $isDev = THEME_MOD === 'development';
    wp_enqueue_script('wp-api');

    if ($isDev) {
        wp_enqueue_script('dev.js');
    } else {
        if (is_admin()) {
            wp_enqueue_script('admin.js');
        } else {
	        wp_enqueue_style('vendors.css');
            wp_enqueue_style('critical.css');
            wp_enqueue_style('main.css');

            wp_enqueue_script('vendors.js');
            wp_enqueue_script('critical.js');
            wp_enqueue_script('main.js');
        }
    }

	$js_data = [
		'ajaxurl'                 => admin_url('admin-ajax.php'),
		'themeUrl'                => get_template_directory_uri(),
		'isHome'                  => is_front_page(),
		'buildDir'                => BUILD_DIR,
		'menu'                    => json_encode(Helper::getNavMenuByName('main')),
		'contacts'                => json_encode(get_fields('contacts')),
		'nonce'                   => wp_create_nonce('wp_rest'),
		'bg_card'                 => Assets::getChunkUrl('assets/images/card_bg.png', THEME_MOD)
	];

	wp_localize_script('critical.js', 'site', $js_data);
    wp_localize_script($isDev ? 'dev.js' : 'main.js', 'site', $js_data);
}, 999);

//wp_add_inline_script( 'wp-api-request', "try { sessionStorage.removeItem( 'wp-api-schema-model' + wpApiSettings.root + wpApiSettings.versionString ); } catch ( err ) {}" );

//Добавляем асинхронность чанкам
Assets::addAsyncChunks('style', ['main.css']);
//Assets::addAsyncChunks('script', ['main.js']);
