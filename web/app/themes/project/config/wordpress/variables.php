<?php
use function Env\env;

/*
|--------------------------------------------------------------------------
| Получение данных из .env файла
|--------------------------------------------------------------------------
*/
$env_files = file_exists(get_template_directory() . '/.env.local')
    ? [ '.env', '.env.local' ]
    : [ '.env' ];

$dotenv = Dotenv\Dotenv::createUnsafeImmutable(get_template_directory(), $env_files, false);

if (file_exists(get_template_directory() . '/.env')) {
    $dotenv->load();
}

/*
|--------------------------------------------------------------------------
| Объявление переменных темы
|--------------------------------------------------------------------------
*/
define('THEME_VERSION', '0.0.1');
define('THEME_DIRNAME', get_template());
define('THEME_ASSETS_URI', get_template_directory_uri() . '/' . env('BUILD_DIR'));
define('THEME_ASSETS_PATH', get_template_directory() . '/' . env('BUILD_DIR'));

define('ASSETS_JSON', THEME_ASSETS_PATH . '/' . 'assets.json');
define('THEME_MOD', env('THEME_MOD'));
define('BUILD_DIR', env('BUILD_DIR') ?? 'dist');

define('DEV_HOST', env('DEV_HOST') ?? 'localhost');
define('DEV_PORT', env('DEV_PORT') ?? '3000');
define('PATH_TO_THEMES', env('PATH_TO_THEMES') ?? '/app/themes');
