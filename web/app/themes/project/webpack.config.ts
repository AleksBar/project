import path from 'path';
import {buildWebpackConfig} from './config/webpack/buildWebpackConfig';
import {BuildEnv, BuildPaths} from './config/webpack/types/config';
import * as dotenv from 'dotenv'

export default (env: BuildEnv) => {
    dotenv.config()

    const mode      = env.mode || 'development';
    const isDev     = mode === 'development';
    const isServe   = env.serve || false;
    const port      = +process.env.DEV_PORT || 3000;
    const domain    = process.env.DOMAIN || null;
    const host      = process.env.DEV_HOST || 'localhost';
    const themeName = path.basename(__dirname)
    const toThemes  = process.env.PATH_TO_THEMES || '/app/themes';
    const buildDir  = process.env.BUILD_DIR || 'dist';
    const allEnv    = {
        ck: process.env.CK,
        cs: process.env.CS,
        userLogin: process.env.USER_LOGIN,
        userPassword: process.env.USER_PASSWORD,
    }
    process.env.NODE_ENV = mode;

    const serveEntry = {
        dev: path.resolve(__dirname, 'src', 'dev.ts'),
    }

    const prodEntry = {
        critical: path.resolve(__dirname, 'src', 'app', 'index.tsx'),
        main:     path.resolve(__dirname, 'src', 'index.ts'),
        // admin:    path.resolve(__dirname, 'src', 'admin', 'index.ts'),
    }

    const paths: BuildPaths = {
        entry:      isServe ? serveEntry : prodEntry,
        build:      path.resolve(__dirname, buildDir),
        html:       path.resolve(__dirname, 'public', 'index.html'),
        src:        path.resolve(__dirname, 'src'),
        toThemes:   toThemes,
        publicPath: domain + ':' + port + toThemes + '/' + themeName + '/' + buildDir + '/',
    };

    return buildWebpackConfig({
        mode,
        paths,
        isDev,
        port,
        domain,
        themeName,
        buildDir,
        isServe,
        host,
        allEnv
    });
};
