<?php

class Helper {
	public static function getTypePage() {
		if ( is_page() ) {
			return 'page';
		}

		if ( is_single() ) {
			return 'single';
		}

		if ( is_admin() ) {
			return 'admin';
		}

		if ( is_404() ) {
			return '404';
		}

		if ( is_privacy_policy() ) {
			return 'privacy_policy';
		}

		return 'page';
	}


	public static function getPostId() {
		global $post;

		if ( $post ) {
			return $post->ID;
		}

		return 0;
	}

	public static function getMenuIdFromLocation( $location ): ?int {
		$locs = get_nav_menu_locations();

		if ( is_array( $locs ) ) {
			return $locs[ $location ];
		}

		return null;
	}

	public static function getRenderResult( callable $callBack, $args = [] ) {
		ob_start();
		call_user_func( $callBack, $args );

		return ob_get_clean();
	}

	public static function replaceItem( $item ): array {
		return [
			'id'       => (int) $item->ID,
			'title'    => $item->title,
			'classes'  => $item->classes,
			'url'      => $item->url,
			'children' => [],
		];
	}

	public static function getNavMenuByName( $name ): array {
		$menu = wp_get_nav_menu_items( $name );
		$list = [];

		foreach ( $menu as $item ) {
			if ( (int) $item->menu_item_parent === 0 ) {
				$list[] = self::replaceItem( $item );
			}
		}


		foreach ( $menu as $item ) {
			$item_id = (int) $item->menu_item_parent;

			if ( $item_id !== 0 ) {
				foreach ( $list as &$parent ) {
					if ( $parent['id'] == $item_id ) {
						$parent['children'][] = self::replaceItem( $item );
					}
				}
			}
		}

		return $list;
	}

	public static function get_global_block($name) {
		$blocks = get_field('blocks', 'global_blocks');

		foreach ($blocks as $block) {
			if ($block['acf_fc_layout'] === $name) {
				return $block;
			}
		}

		return null;
	}
}
