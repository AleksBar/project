<?php
function fn_get_menu($name)
{
    $buffer = wp_get_nav_menu_items($name);

    $menu_template = [];

    foreach ($buffer as $item) {
        if (!(!!$item->menu_item_parent)) {
            $menu_template[ $item->ID ] = [
                'data'  => $item,
                'title' => $item->title,
                'items' => [],
                'url'   => $item->url
            ];
        } else {
            $menu_template[ $item->menu_item_parent ]['items'][ $item->ID ] = [
                'data'  => $item,
                'title' => $item->post_title,
                'url'   => $item->url
            ];
        }
    }

    return $menu_template;
}
