<?php

/**
 * Cass for register assets webpack
 */
class Assets
{
    public static array $assets = [];

    public static array $params = [
        'mode'            => 'production',
        'devToThemesUrl'  => null,
        'prodToThemesUrl' => null,
        'themeName'       => null,
        'buildDir'        => 'dist',
    ];

    public static function init(string $file, array $params = [])
    {

        // Получаем данные из файла assets.json
        if (file_exists($file)) {
            self::$assets = (array) json_decode(file_get_contents($file));
        }

        if ($params) {
            self::$params = array_merge(self::$params, $params);
        }
    }

    public static function registerChunks()
    {
        foreach (self::$assets as $name => $url) {
            $data = explode('.', $name);
            $ext  = end($data);

            // Получение полного пути до чанка
            $url = self::getChunkUrl($name, self::$params['mode']);

            if ($ext === 'css') {
                wp_register_style($name, $url, [], null);
            } elseif ($ext === 'js') {
                wp_register_script($name, $url, [ 'jquery', 'wp-api' ], null, true);
            }
        }
    }

    public static function getChunkUrl($name, $mode = 'development'): ?string
    {
        if (! empty(self::$assets[ $name ])) {
            if ($mode === 'development') {
                return self::$params['devToThemesUrl'] . '/' . self::$params['themeName'] . "/" . self::$params['buildDir'] . "/" . self::$assets[ $name ];
            } else {
                return self::$params['prodToThemesUrl'] . self::$params['themeName'] . "/" . self::$params['buildDir'] . "/" . self::$assets[ $name ];
            }
        } else {
            return null;
        }
    }

	public static function addAsyncChunks($type, $chunks = []): void {
		if ($type === 'script') {
			add_filter('script_loader_tag', function ($tag, $handle) use ($chunks, $type) {
				if (in_array($handle, $chunks)) {
					return str_replace(' src', ' defer="defer" src', $tag);
				} else {
					return $tag;
				}
			}, 10, 2);
		} else {
			add_filter('style_loader_tag', function ($tag, $handle) use ($chunks, $type) {
				if (in_array($handle, $chunks)) {
					return str_replace("media='all'", 'media=\'print\' onload="this.media=\'screen\'"', $tag);
				} else {
					return $tag;
				}
			}, 10, 2);
		}
	}
}
